package models

import (
	"github.com/jinzhu/gorm"
)

type Invoice struct {
	gorm.Model
	InvoiceNo    string
	NameCompany  string
	NameCustomer string
	IsuueDate    string
	DueDate      string
}

type InvoiceService interface {
	CreateInvoice(invoice *Invoice) error
	ListInvoice() ([]Invoice, error) 
}

type invoiceGorm struct {
	db *gorm.DB
}

func NewInvoiceService(db *gorm.DB) InvoiceService {
	return &invoiceGorm{db}

}

func (gg *invoiceGorm) CreateInvoice(invoice *Invoice) error {
	return gg.db.Create(invoice).Error
}

func (gg *invoiceGorm) ListInvoice() ([]Invoice, error) {
	ListTable := []Invoice{}
	if err := gg.db.Find(&ListTable).Error; err != nil {
		return nil, err
	}
	return ListTable, nil
}
