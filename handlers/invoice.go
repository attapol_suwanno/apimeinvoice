package handlers

import (
	"apiinvoice/models"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)
type InvoiceCreate struct{
	InvoiceNo string 
	NameCompany string
	NameCustomer string
	IsuueDate string
	DueDate string
}

type Invoicehandler struct {
	gs models.InvoiceService
}


func NewInvoiceHandler(gs models.InvoiceService) *Invoicehandler {
	return &Invoicehandler{gs}
}


func (gh *Invoicehandler) CreateInvoice(c *gin.Context) {
	data := new(InvoiceCreate)
	if err := c.BindJSON(&data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("get data %v",data)

	invoice := new(models.Invoice)
	invoice.InvoiceNo = data.InvoiceNo
	invoice.NameCompany = data.NameCompany
	invoice.NameCustomer = data.NameCustomer
	invoice.IsuueDate = data.IsuueDate
	invoice.DueDate = data.DueDate
	if err := gh.gs.CreateInvoice(invoice); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"id": invoice.ID,
		"InvoiceNo":   invoice.InvoiceNo,
		"NameCompany": invoice.NameCompany,
		"NameCustomer": invoice.NameCustomer,
		"IsuueDate": invoice.IsuueDate,
		"DueDate": invoice.DueDate,
	})

}

func (gh *Invoicehandler) InvoiceList(c *gin.Context) {
	data, err := gh.gs.ListInvoice()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	invoice := []models.Invoice{}
	for _, a := range data {
		invoice = append(invoice, models.Invoice{
			InvoiceNo: a.InvoiceNo,
			NameCompany: a.NameCompany,
			NameCustomer: a.NameCustomer,
			IsuueDate: a.IsuueDate,
			DueDate: a.DueDate,
		})
	}

	c.JSON(http.StatusOK, invoice)
}