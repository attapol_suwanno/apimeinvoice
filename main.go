package main

import (
	"apiinvoice/handlers"
	"apiinvoice/models"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	db, err := gorm.Open("mysql", "root:password@tcp(127.0.0.1:3306)/meinvoicedb?charset=utf8&parseTime=True")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.LogMode(true) // dev only!!!!

	if err := db.AutoMigrate(&models.Invoice{}).Error; err != nil {
		log.Fatal(err)
	}
	r := gin.Default()
	gs := models.NewInvoiceService(db)
	gh := handlers.NewInvoiceHandler(gs)

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"}

	r.POST("/invoice", gh.CreateInvoice)
	r.GET("/invoice", gh.InvoiceList)

	r.Run()
}
